package com.subhashree.oneToOneWithMigration;

import com.subhashree.oneToOneWithMigration.entity.Course;
import com.subhashree.oneToOneWithMigration.entity.CourseMaterial;
import com.subhashree.oneToOneWithMigration.repository.CourseMaterialRepository;
import com.subhashree.oneToOneWithMigration.repository.CourseRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSeeder {


    @Bean
    CommandLineRunner initDatabase(CourseRepository courseRepository, CourseMaterialRepository courseMaterialRepository) {
        return args -> {
            Course courseOne = Course.builder()
//                    .courseId(1)
                    .title("DSA")
                    .credit(6)
                    .build();

            CourseMaterial courseMaterialOne = CourseMaterial.builder()
//                    .courseMaterialId(1)
                    .url("google.com")
                    .course(courseOne)
                    .build();
//                courseRepository.save(course);
            if (courseMaterialRepository.findById(1).isEmpty())
                courseMaterialRepository.save(courseMaterialOne);

            Course courseTwo = Course.builder()
//                    .courseId(2)
                    .title("Java")
                    .credit(6)
                    .build();

            CourseMaterial courseMaterialTwo = CourseMaterial.builder()
//                    .courseMaterialId(2)
                    .url("telusko.com")
                    .course(courseTwo)
                    .build();

            if (courseMaterialRepository.findById(2).isEmpty())
                courseMaterialRepository.save(courseMaterialTwo);


        };
    }
}




