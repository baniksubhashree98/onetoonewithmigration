package com.subhashree.oneToOneWithMigration;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EntityScan("entity")
@SpringBootApplication
public class OneToOneWithMigrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(OneToOneWithMigrationApplication.class, args);

    }

}
