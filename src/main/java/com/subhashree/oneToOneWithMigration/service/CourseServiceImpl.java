package com.subhashree.oneToOneWithMigration.service;

import com.subhashree.oneToOneWithMigration.entity.Course;
import com.subhashree.oneToOneWithMigration.repository.CourseMaterialRepository;
import com.subhashree.oneToOneWithMigration.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService{
    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<Course> fetchCourseList() {
        return courseRepository.findAll();
    }

    @Override
    public Course fetchCourseByCourseMaterialId(Integer id) {
        return courseRepository.findCourseByCourseMaterialId(id);
    }

    @Override
    public List<Course> fetchCourseByCourseMaterialUrl(String url) {
        return courseRepository.findCourseByCourseMaterialUrl(url);
    }
}
