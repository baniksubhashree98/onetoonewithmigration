package com.subhashree.oneToOneWithMigration.service;

import com.subhashree.oneToOneWithMigration.entity.Course;

import java.util.List;

public interface CourseService {
    List<Course> fetchCourseList();

    Course fetchCourseByCourseMaterialId(Integer id);

    List<Course> fetchCourseByCourseMaterialUrl(String url);
}
