package com.subhashree.oneToOneWithMigration.service;

import com.subhashree.oneToOneWithMigration.entity.CourseMaterial;

import java.util.List;

public interface CourseMaterialService {
    List<CourseMaterial> fetchCourseMaterialList();

    CourseMaterial fetchCourseMaterialByCourseId(Integer id);

    List<CourseMaterial> fetchCourseMaterialByCourseCredit(Integer credit);

    void deleteCourseMaterialById(Integer courseMaterialId);

    void deleteCourseMaterialByUrl(String url);
}
