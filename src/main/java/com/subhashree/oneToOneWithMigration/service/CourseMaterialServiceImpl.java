package com.subhashree.oneToOneWithMigration.service;

import com.subhashree.oneToOneWithMigration.entity.CourseMaterial;
import com.subhashree.oneToOneWithMigration.repository.CourseMaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseMaterialServiceImpl implements CourseMaterialService{
    @Autowired
    private CourseMaterialRepository courseMaterialRepository;


    @Override
    public List<CourseMaterial> fetchCourseMaterialList() {
        return courseMaterialRepository.findAll();
    }

    @Override
    public CourseMaterial fetchCourseMaterialByCourseId(Integer id) {
        return courseMaterialRepository.findCourseMaterialByCourseId(id);
    }

    @Override
    public List<CourseMaterial> fetchCourseMaterialByCourseCredit(Integer credit) {
        return courseMaterialRepository.findCourseMaterialByCourseCredit(credit);
    }

    @Override
    public void deleteCourseMaterialById(Integer courseMaterialId) {
        courseMaterialRepository.deleteById(courseMaterialId);
    }

    @Override
    public void deleteCourseMaterialByUrl(String url) {
        courseMaterialRepository.deleteByUrl(url);
    }
}
