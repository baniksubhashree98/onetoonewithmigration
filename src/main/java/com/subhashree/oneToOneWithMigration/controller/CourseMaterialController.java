package com.subhashree.oneToOneWithMigration.controller;

import com.subhashree.oneToOneWithMigration.entity.CourseMaterial;
import com.subhashree.oneToOneWithMigration.service.CourseMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseMaterialController {
    @Autowired
    private CourseMaterialService courseMaterialService;

    @GetMapping("/courseMaterial")
    public List<CourseMaterial> fetchCourseMaterialList(){
        return courseMaterialService.fetchCourseMaterialList();
    }

    @GetMapping("/courseMaterial/courseId/{id}")
    public CourseMaterial fetchCourseMaterialByCourseId(@PathVariable("id") Integer id) {
        return courseMaterialService.fetchCourseMaterialByCourseId(id);
    }

    @GetMapping("/courseMaterial/courseCredit/{credit}")
    public List<CourseMaterial> fetchCourseMaterialByCourseCredit(@PathVariable("credit") Integer credit) {
        return courseMaterialService.fetchCourseMaterialByCourseCredit(credit);
    }

    @DeleteMapping("/courseMaterial/delete/{id}")
    public String deleteCourseMaterialById(@PathVariable("id") Integer courseMaterialId) {
        courseMaterialService.deleteCourseMaterialById(courseMaterialId);
        return "Course material deleted successfully!!";
    }

    @DeleteMapping("/courseMaterial/delete/url/{courseMaterialUrl}")
    public String deleteCourseMaterialByUrl(@PathVariable("courseMaterialUrl") String url) {
        courseMaterialService.deleteCourseMaterialByUrl(url);
        return "Course material deleted successfully!!";
    }

}
