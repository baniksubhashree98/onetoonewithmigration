package com.subhashree.oneToOneWithMigration.controller;

import com.subhashree.oneToOneWithMigration.entity.Course;
import com.subhashree.oneToOneWithMigration.entity.CourseMaterial;
import com.subhashree.oneToOneWithMigration.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping("/course")
    public List<Course> fetchCourseList(){
        return courseService.fetchCourseList();
    }

    @GetMapping("/course/courseMaterialId/{id}")
    public Course fetchCourseByCourseMaterialId(@PathVariable("id") Integer id) {
        return courseService.fetchCourseByCourseMaterialId(id);
    }

    @GetMapping("/course/courseMaterialUrl/{url}")
    public List<Course> fetchCourseByCourseMaterialUrl(@PathVariable("url") String url) {
        return courseService.fetchCourseByCourseMaterialUrl(url);
    }
}
