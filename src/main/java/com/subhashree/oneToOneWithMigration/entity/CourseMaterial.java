package com.subhashree.oneToOneWithMigration.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@Table(name = "course_material")
@ToString(exclude="course")

public class CourseMaterial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private Integer courseMaterialId;

    @Column(nullable = false)
    @JsonProperty
    private String url;

    @OneToOne(
//            on changing the cascade from all to the following, on deleting course material, corresponding course isn't deleted
            cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH},
            //todo-make this lazy and uncomment @JsonIgnoreProperties("hibernateLazyInitializer")
            fetch = FetchType.EAGER,
            optional = false
    )
    @JoinColumn(
            name = "course_id",
            referencedColumnName = "courseId"
    )
    @JsonManagedReference
//    @JsonIgnoreProperties("hibernateLazyInitializer")
//    @JsonBackReference
//    @JsonProperty
    private Course course;
}

