package com.subhashree.oneToOneWithMigration.repository;


import com.subhashree.oneToOneWithMigration.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {
    @Query("select c from Course c where c.courseMaterial.courseMaterialId=?1")
    Course findCourseByCourseMaterialId(Integer id);

    @Query("select c from Course c where c.courseMaterial.url=?1")
    List<Course> findCourseByCourseMaterialUrl(String url);

}
