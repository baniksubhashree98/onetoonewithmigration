package com.subhashree.oneToOneWithMigration.repository;


import com.subhashree.oneToOneWithMigration.entity.CourseMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface CourseMaterialRepository extends JpaRepository<CourseMaterial, Integer> {
    @Query(value = "select * from course_material c where c.course_id=:courseId", nativeQuery = true)
    CourseMaterial findCourseMaterialByCourseId(@Param("courseId") Integer id);

    @Query("select c from CourseMaterial c where c.course.credit=?1")
    List<CourseMaterial> findCourseMaterialByCourseCredit(Integer credit);

    @Transactional
    @Modifying
    @Query(value = "delete from course_material c where c.url=:url", nativeQuery = true)
    void deleteByUrl(@Param("url") String url);
}
